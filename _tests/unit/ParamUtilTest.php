<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ParamUtilTest tests the utils\ParamUtil class static functions.
 * There should be a test for each function with as many asserts as needed
 * to prove the function behaves as designed/expected.
 * 
 *
 * @author Arthur
 */
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use utils\ParamUtil;

final class ParamUtilTest extends TestCase {

    public function testSourceCheck(): void {
        $excepted = false;
        try {
            ParamUtil::SourceCheck("this is a string");
            $excepted = true;
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'is not an array', $e->getMessage());
        }
        $this->assertFalse($excepted, 'testSourceCheck: string should cause exception');

        $excepted = false;
        try {
            ParamUtil::SourceCheck(1234);
            $excepted = true;
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'is not an array', $e->getMessage());
        }
        $this->assertFalse($excepted, 'testSourceCheck: integer should cause exception');

        $excepted = false;
        try {
            ParamUtil::SourceCheck(1.234);
            $excepted = true;
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'is not an array', $e->getMessage());
        }
        $this->assertFalse($excepted, 'testSourceCheck: float should cause exception');

        $excepted = false;
        try {
            $testObj = (object) array("key" => "val1", "key2" => "val2");
            ParamUtil::SourceCheck($testObj);
            $excepted = true;
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'is not an array', $e->getMessage());
        }
        $this->assertFalse($excepted, 'testSourceCheck: object should cause exception');


        try {
            $linearTestArray = array('apples', 'peaches', 'pumpkin');
            ParamUtil::SourceCheck($linearTestArray);

            $assocTestArray = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4);
            ParamUtil::SourceCheck($assocTestArray);
        } catch (\Exception $e) {
            $this->assertFalse(true, 'testSourceCheck: Unexpected exception received for valid arrays;' . $e->getMessage());
        }
    }

    public function testHas(): void {
        // Simulate examples in Usage example
        // ?&export_type=pdf
        $request = array('export_type' => 'pdf');
        $hasExportType = ParamUtil::Has($request, 'export_type');
        $this->assertTrue($hasExportType, 'export_type should have existed in test array');

        //?&paramx=1
        $request = array('paramx' => 1);
        $hasExportType = ParamUtil::Has($request, 'export_type', 'csv');
        $this->assertFalse($hasExportType, 'export_type should not have existed in test array');
    }

    public function testGet(): void {
        // ?action=list&data={"json":{"a":1,"b":2,"c":3}}&data2={'json':{"d":4,"e":5,"f":6}]
        $jsonString1 = <<<JSON
{"json":{"a":1,"b":2,"c":3}}
JSON;
        $jsonString2 = <<<JSON
{"json":{"d":4,"e":5,"f":6}}
JSON;
        $request = array('action' => 'list', 'data' => $jsonString1, 'data2' => $jsonString2);
        $action = ParamUtil::Get($request, 'action');
        $this->assertEquals($action, 'list', 'testGet: action should have been "list"');

        $data1 = ParamUtil::Get($request, 'data', null, true);
        $this->assertEquals($data1['b'], 2, 'testGet: data1["b"] should be 2');
        $data2 = ParamUtil::Get($request, 'data2', null, true);
        $this->assertEquals($data2['f'], 6, 'testGet: data2["f"] should be 6');
        $data3 = ParamUtil::Get($request, 'data3', null, true);
        $this->assertNull($data3, 'testGet: data3 should be null');
        $data4 = ParamUtil::Get($request, 'data', null, false);
        $this->assertEquals($data4, $jsonString1, 'data4 should be the whole {"json": param string');
    }

    public function testGetOne(): void {
        //?p=do something
        $request = array('p' => 'do something');
        $param = ParamUtil::GetOne($request, 'p', 'parameter');
        $this->assertEquals($param, 'do something');

        //?parameter=do something
        $request = array('parameter' => 'do something');
        $param = ParamUtil::GetOne($request, 'p', 'parameter');
        $this->assertEquals($param, 'do something');

        $param = ParamUtil::GetOne($_REQUEST, 'p', 'param');
        $this->assertNull($param);
    }

    public function testGetValues(): void {
        //?param1=val1,param2=val2
        $request = array('param1' => 'val1', 'param2' => 'val2');
        $params = ParamUtil::GetAllValues($request, 'param1', 'param2', 'param3');
        $this->assertEquals($params['param1'], 'val1', 'testGetValue: param1 should be val1');
        $this->assertEquals($params['param2'], 'val2', 'testGetValue: param2 should be val2');
        $this->assertFalse(isset($params['param3']), 'testGetValue: param3 should not be set');
    }

    public function testGetAllValues(): void {
        //?param1=val1,param2=val2
        $request = array('param1' => 'val1', 'param2' => 'val2');
        $params = ParamUtil::GetAllValues($request, 'param1', 'param2', 'param3');
        $this->assertEquals($params['param1'], 'val1', 'testGetAll: param1 should be val1');
        $this->assertEquals($params['param2'], 'val2', 'testGetAll: param2 should be val2');
        $this->assertNull($params['param3'], 'testGetAll: parm3 should be null');
    }

    public function testForceOne(): void {
        $request = array();
        $authenticate = ParamUtil::ForceOne($request, 'authenticated', true);
        $this->assertEquals($authenticate, ParamUtil::Get($request, 'authenticated'));
    }

    public function testGetJSON(): void {
        //?&action=list&data={"a":1,"b":2,"c":3}&bad={"a":"b":2,"c"}
        $goodJSONData = <<<JSON
{"a":1,"b":2,"c":3}
JSON;

        $badJSONData = <<<JSONBAD
{"a":"b":2,"c"}
JSONBAD;
        $request = array('action' => 'list', 'data' => $goodJSONData, 'bad' => $badJSONData);
        $data = ParamUtil::GetJSON($request, 'data');

        $this->assertEquals($data["c"], 3, "testGetJSON: data[c] should be 3");
        $data2 = ParamUtil::GetJSON($request, 'data2', 'doh!');
        $data3 = ParamUtil::GetJSON($request, 'data3');
        $bad = ParamUtil::GetJSON($request, 'bad', 'that was bad');


        $this->assertEquals($data2, 'doh!', 'testGetJSON: data2 should be provided default value of doh!');
        $this->assertIsArray($data3, 'testGetJSON: data3 should be default array');
        $this->assertCount(0, $data3, 'testJSON: data3 array should be empty');
        $this->assertNull($bad, 'testJSON: badJSONData was malformed JSON and should be false');
    }

    public function testGetBoolean() {
        //?is_filtering=true&is_listing=t&is_sorting=1
        $request = array('is_filtering' => 'true', 'is_listing' => 't', 'is_sorting' => 1);
        $request['yup'] = true;
        $request['nope'] = false;
        $request['is_a_thing'] = 'f';
        $request['is_tracking'] = 'maybe';
        $request['numeric_false'] = 0;

        list($isListing, $isFiltering, $isSorting, $yup, $nope, $isAThing, $isTracking, $numericFalse, $notAThing) = ParamUtil::GetBoolean($request, 'is_listing', 'is_filtering', 'is_sorting', 'yup', 'nope', 'is_a_thing', 'is_tracking', 'numeric_false', 'not_a_thing');


        $this->assertTrue($isFiltering);
        $this->assertTrue($isListing);
        $this->assertTrue($isSorting);
        $this->assertTrue($yup);
        $this->assertFalse($nope);
        $this->assertFalse($isAThing);
        $this->assertTrue($isTracking);
        $this->assertFalse($numericFalse);
        $this->assertFalse($notAThing);
        $this->assertTrue($isFiltering && $isListing && $isSorting && $yup && $isTracking);
        $this->assertTrue(!$nope && !$isAThing && !$numericFalse && !$notAThing);
    }

    public function testGetList(): void {
        //?names=Larry,Moe,Curly
        $request = array('names' => 'Larry,Moe,Curly');
        $names = ParamUtil::GetList($request, ',', 'names');
        $this->assertIsArray($names);
        $this->assertCount(3, $names);
        $this->assertEquals($names[0], 'Larry');
        $this->assertEquals($names[1], 'Moe');
        $this->assertEquals($names[2], 'Curly');
    }

    public function testRequires(): void {
        //?param=someval
        $request = array('param' => 'someval');
        $excepted = false;
        try {
            list($param, $param2) = ParamUtil::Requires($request, 'param', 'param2');
        } catch (\Exception $e) {
            $excepted = true;
            $message = $e->getMessage();
            $this->assertTrue(stripos($message, 'Missing required parameters:') > -1);
            $this->assertTrue(stripos($message, 'param2') > -1);
        }
        $this->assertTrue($excepted, 'testRequires: should have recieved a missing parameter exception');
        list($param) = ParamUtil::Requires($request, 'param');
        $this->assertEquals($param, 'someval');
    }

    public function testRequiresOne(): void {
        //?n=10&q=1
        $request = array('n' => 10, 'q' => 1);
        $numLines = ParamUtil::RequiresOne($request, 'n', 'num_lines');
        $this->assertEquals($numLines, 10);

        $numLines = ParamUtil::RequiresOne($request, 'n', 'num_lines');
        $this->assertEquals($numLines, 10);

        //
        $request = array('number_of_lines' => 10, "q" => '1');
        $excepted = false;

        try {
            $numLines = ParamUtil::RequiresOne($request, 'n', 'num_lines');
        } catch (\Exception $e) {
            $excepted = true;
            $message = $e->getMessage();
            $this->assertTrue(stripos($message, 'Missing required parameters: requires one of') === 0);
            $this->assertTrue(stripos($message, 'n,num_lines') > 0);
        }
        $this->assertTrue($excepted, 'testRequiresOne: a missing required parameters exception should have occurred');
    }

    public function testRequireJSON(): void {
        //?&action=list&data={"a":1,"b":2,"c":3}&bad={"a":"b":2,"c"}
        $goodJSONData = <<<JSON
{"a":1,"b":2,"c":3}
JSON;

        $badJSONData = <<<JSONBAD
{"a":"b":2,"c"}
JSONBAD;
        $request = array('action' => 'list', 'data' => $goodJSONData, 'bad' => $badJSONData);
        $data = ParamUtil::RequireJSON($request, 'data');
        $this->assertEquals($data["c"], 3, "testGetJSON: data[c] should be 3");
        $bad = ParamUtil::GetJSON($request, 'bad', 'that was bad');
        $this->assertNull($bad, 'testJSON: badJSONData was malformed JSON and should be false');

        $excepted = false;
        try {

            $data2 = ParamUtil::RequireJSON($request, 'data2');
        } catch (\Exception $e) {
            $excepted = true;
            $message = $e->getMessage();
            $this->assertTrue(stripos($message, 'Missing required parameters: requires one of ') === 0);
            $this->assertTrue(stripos($message, 'data2') > 0);
        }
        $this->assertTrue($excepted, 'testRequireJSON: attempt to get data2 should have resulted in missing reuired parameters exception');
    }

    public function testPrune() {
        //?action=list&target=obj1&tracking=12345&cachebust=42
        $request = array('action' => 'list', 'target' => 'obj1', 'tracking' => '12345', 'cachebust' => '42');
        ParamUtil::Prune($request, 'tracking', 'cachebust');
        $this->assertFalse(isset($request['tracking']));
        $this->assertFalse(isset($request['cachebust']));
        $excepted = false;
        try {
            $fruit = array('apples', 'peaches', 'pumpkin', 'pie');
            ParamUtil::Prune($fruit, 'pie');
        } catch (Exception $e) {
            $excepted = true;
            $message = $e->getMessage();
            $this->assertEquals($message, 'Assoc array expected: consider using PruneIndexed instead');
        }
        $this->assertTrue($excepted);
    }

    public function testPruneIndexed(): void {
        $fruit = array("apple", "peaches", "pumpkin", "pie", "cheese");
        ParamUtil::PruneIndexed($fruit, [4, 3]);
        $this->assertCount(3, $fruit);
        $this->assertTrue(in_array('apple', $fruit));
        $this->assertTrue(in_array('peaches', $fruit));
        $this->assertTrue(in_array('pumpkin', $fruit));
        $this->assertFalse(in_array('pie', $fruit));
        $this->assertFalse(in_array('cheese', $fruit));
        $excepted = false;
        $fruit = array('a' => 'apples', 'p1' => 'peaches', 'p2' => 'pumpkin', 'p3' => 'pie');


        try {
            ParamUtil::PruneIndexed($fruit, 'pie');
        } catch (Exception $e) {
            $excepted = true;
            $message = $e->getMessage();
            $this->assertEquals($message, 'Linear array expected: Consider using Prune instead');
        }
        $this->assertTrue($excepted);
    }

    public function testPruneIndexedValues(): void {
        $fruit = array("apple", "peaches", "pumpkin", "pie", "cheese");
        ParamUtil::PruneIndexedValues($fruit, 'pie', 'cheese');
        $this->assertCount(3, $fruit);
        $this->assertTrue(in_array('apple', $fruit));
        $this->assertTrue(in_array('peaches', $fruit));
        $this->assertTrue(in_array('pumpkin', $fruit));
        $this->assertFalse(in_array('pie', $fruit));
        $this->assertFalse(in_array('cheese', $fruit));
        $excepted = false;
        $fruit = array('a' => 'apples', 'p1' => 'peaches', 'p2' => 'pumpkin', 'p3' => 'pie');
        try {

            ParamUtil::PruneIndexedValues($fruit, 'pie');
        } catch (Exception $e) {
            $excepted = true;
            $message = $e->getMessage();
            $this->assertEquals($message, 'Linear array expected: Consider using Prune instead');
        }
        $this->assertTrue($excepted);
    }

    public function testGetAllAliasedValues(): void {
        $record = array('f1' => 1000.00, 'q2' => 2000.0);
        $outRecord = ParamUtil::GetAllAliasedValues($record, 'f1:First quarter earnings', 'q2:Second quarter earnings', 'q3:Third quarter earnigns');
        $this->assertIsArray($outRecord);
        
        $this->assertArrayHasKey('First quarter earnings', $outRecord);
        $this->assertArrayHasKey('Second quarter earnings', $outRecord);
        $this->assertArrayHasKey('Third quarter earnigns', $outRecord);        
        $this->assertNull($outRecord['Third quarter earnigns']);
        $this->assertArrayNotHasKey('f1', $outRecord);
        $this->assertArrayNotHasKey('q2', $outRecord);
        $this->assertArrayNotHasKey('q3', $outRecord);
        
        
        // Simulate modification by some other functionality or as data submitted to server:
        $inRecord = $outRecord;
        $inRecord['Second quarter earnings'] = 1500.00;
        $recordToStore = ParamUtil::GetAllAliasedValues($inRecord,'First quarter earnings:f1','Second quarter earnings:q2','Third quarter earnigns:q3');
        $this->assertIsArray($recordToStore);
        $this->assertArrayHasKey('f1', $recordToStore);
        $this->assertArrayHasKey('q2', $recordToStore);
        $this->assertArrayHasKey('q3', $recordToStore);
        $this->assertNull($recordToStore['q3']);
        $this->assertArrayNotHasKey('First quarter earnings', $recordToStore);
        $this->assertArrayNotHasKey('Second quarter earnings', $recordToStore);
        $this->assertArrayNotHasKey('Third quarter earnigns', $recordToStore); 
        
    }
    
    public function testGetAliasedValues():void {
        $record = array('f1' => 1000.00, 'q2' => 2000.0);
        $outRecord = ParamUtil::GetAliasedValues($record, 'f1:First quarter earnings', 'q2:Second quarter earnings', 'q3:Third quarter earnigns');
        $this->assertIsArray($outRecord);
      
        $this->assertArrayHasKey('First quarter earnings', $outRecord);
        $this->assertArrayHasKey('Second quarter earnings', $outRecord);
        $this->assertArrayNotHasKey('Third quarter earnigns', $outRecord);        
        $this->assertArrayNotHasKey('f1', $outRecord);
        $this->assertArrayNotHasKey('q2', $outRecord);
        $this->assertArrayNotHasKey('q3', $outRecord);
        
        
        // Simulate modification by some other functionality or as data submitted to server:
        $inRecord = $outRecord;
        $inRecord['Second quarter earnings'] = 1500.00;
        $recordToStore = ParamUtil::GetAliasedValues($inRecord,'First quarter earnings:f1','Second quarter earnings:q2','Third quarter earnigns:q3');
        $this->assertIsArray($recordToStore);
        $this->assertArrayHasKey('f1', $recordToStore);
        $this->assertArrayHasKey('q2', $recordToStore);
        $this->assertArrayNotHasKey('q3', $recordToStore);
        $this->assertArrayNotHasKey('First quarter earnings', $recordToStore);
        $this->assertArrayNotHasKey('Second quarter earnings', $recordToStore);
        $this->assertArrayNotHasKey('Third quarter earnigns', $recordToStore); 
    }
    
    public function testRequireAliasedValues():void {
        $record = array('f1' => 1000.00, 'q2' => 2000.0);
        $outRecord = ParamUtil::RequireAliasedValues($record, 'f1:First quarter earnings', 'q2:Second quarter earnings');
        $this->assertIsArray($outRecord);
        $this->assertArrayHasKey('First quarter earnings', $outRecord);
        $this->assertArrayHasKey('Second quarter earnings', $outRecord);
        
        $excepted = false;
        try {
            $outRecord = ParamUtil::RequireAliasedValues($record, 'f1:First quarter earnings', 'q2:Second quarter earnings', 'q3:Third quarter earnigns');
        } catch( \Exception $e) {
            $excepted = true;
            $message = $e->getMessage();
            $this->assertTrue(stripos($message,'Missing required parameters: ' )=== 0);
            $this->assertTrue(stripos($message,'q3')>-1);
        }        
        $this->assertTrue($excepted);
    }
    
    public function testParseParams():void {
        $url = "/param1:val1/param2:val%202/";
        
        $params = ParamUtil::ParseParams($url);
        
        $this->assertIsArray($params);
        $this->assertArrayHasKey('param1',$params);
        $this->assertArrayHasKey('param2',$params);
        $this->assertEquals($params['param1'],'val1');
        $this->assertEquals($params['param2'],'val 2');
        
    }
    
    public function testIsAssoc():void {
        $test = array('a'=>1,0=>'stuff');
        $this->assertTrue(ParamUtil::IsAssoc($test));
        
        $test = array('stuff','morestuff');
        $this->assertFalse(ParamUtil::IsAssoc($test));
    }
    
    public function testGetSubValues():void {
        $data=array();
        $data[] = array('id'=>'12340','field1'=>'val1');
        $data[] = array('id'=>'12341','field1'=>'val2');
        $data[] = array('id'=>'12342','field1'=>'val3');
        
        $ids = ParamUtil::GetSubValues($data,'id');
        $this->assertIsArray($data);
        $this->assertCount(3,$data);
        $this->assertFalse(ParamUtil::IsAssoc($ids));
        $this->assertEquals($ids[0],'12340');
        $this->assertEquals($ids[1],'12341');
        $this->assertEquals($ids[2],'12342');
        
        $data=array();
        $data[] = array('id'=>'12340','field1'=>array('key'=>'val'));
        $data[] = array('id'=>'12341','field1'=>array('key'=>'val2'));
        $data[] = array('id'=>'12342','field1'=>array('key'=>'val3'));
        
        $ids = ParamUtil::GetSubValues($data,'field1.key');
        $this->assertIsArray($data);
        $this->assertCount(3,$data);
        $this->assertFalse(ParamUtil::IsAssoc($ids));
        $this->assertEquals($ids[0],'val');
        $this->assertEquals($ids[1],'val2');
        $this->assertEquals($ids[2],'val3');                
    }
    
    public function testSubsetAssocArray():void {
        //?action=list&target=obj1&tracking=12345&cachebust=42
        $request = array('action'=>'list','target'=>'obj1','tracking'=>'12345','cachebust'=>'42');
        $params = ParamUtil::SubsetAssocArray($request,array('action','target'));
        $this->assertIsArray($params);
        $this->assertTrue(ParamUtil::IsAssoc($params));
        $this->assertArrayHasKey('action',$params);
        $this->assertArrayHasKey('target',$params);
        $this->assertArrayNotHasKey('tracking',$params);
        $this->assertArrayNotHasKey('cachebust',$params);
    }
    
    public function testBoolToTF():void {
        $isValid = true;
        $t_or_f = ParamUtil::BoolToTF($isValid);
        $this->assertEquals('t',$t_or_f);
        
        $isValid = false;
        $t_or_f = ParamUtil::BoolToTF($isValid);
        $this->assertEquals('f',$t_or_f);
        
    }
    
    public function testGetFormat():void {
        //?format=html
        $request = array('format'=>'html');
        $format = ParamUtil::GetFormat($request);
        $this->assertEquals('html',$format);
        
        //?action=list
        $request = array('action'=>'list');
        $format = ParamUtil::GetFormat($request);
        $this->assertEquals('json',$format);
        
        
        
        
    }
    
    
}

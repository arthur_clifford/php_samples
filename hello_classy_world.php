<?php
 require_once('global.inc.php');
 use \utils\ParamUtil;
 
 class Howdy {
 
    public function sayHello($args) {
       $who = ParamUtil::GetOne($args,'x','who');
       if(is_null($who)) $who = 'world';

       echo "Hello $who";
    }
 }

 $howdy = new Howdy();
 $howdy->sayHello($_REQUEST);
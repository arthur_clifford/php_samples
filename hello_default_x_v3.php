<?php
 require_once('global.inc.php');
 use \utils\ParamUtil;
 
 
 function sayHello($args) {
    $who = ParamUtil::GetOne($_REQUEST,'x','who');
    if(is_null($who)) $who = 'world';

    echo "Hello $who";
 }

sayHello($_REQUEST);
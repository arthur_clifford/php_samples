<?php

namespace utils;

/**
 * Adaptation ParamUtil object developed for for SimpleLayers.com.
 *
 * ParamUtil provides functionality for working with
 * assoc arrays of parameters and for getting data or defaults as needed.
 *
 * 
 * @author Arthur Clifford
           
 */
class ParamUtil {
    /**
     * More of an internal function used by other static functions in this class
     * to verify the provided source parameters are an array.
     * 
     * Usage:
     * ```php
     * ParamUtil::SourceCheck($args);
     * // note if you get past the source check call you have an array.
     * ```
     * @param array $src
     * @throws \Exception "is not an array" thrown if src is not an array.
     */
    static function SourceCheck($src) {
        if (!is_array($src))
            throw new \Exception('is not an array');        
    }

    /**
     * Determine whether a property exists in the source.
     * 
     * Usage:
     * ```php
     * ...?&export_type=pdf
     * $hasExportType = ParamUtil::Has($_REQUEST,'export_type');
     * echo $hasExportType ? 'true' : 'false';
     * // Output: true
     * 
     * ...?paramx=1
     * $hasExportType = ParamUtil::Has($_REQUEST,'export_type','csv');
     * echo $hasExportType ? 'true' : 'false';
     * // Output:false
     * ```
     * 
     * @param array $src Associative array with parameters
     * @param string $param key to test for in assoc source
     * @return bool true if present false if not.
     * @throws \Exception "is not an array" thrown if src is not an array.
     * 
     */
    public static function Has($src, $param) {
        return isset($src[$param]);
    }

    /**
     * Get a property from the source object or a default value if the property 
     * is not found, optionally detect json objects.
     * 
     * Usage:
     * ```php
     * ?action=list&data={"json":{"a":1,"b":2,"c":3}}&data2={'json':{"d":4,"e":5,"f":6}}
     * $action = ParamUtil::Get($_REQUEST,'action');
     * echo $action;
     * Output:list
     * 
     * $data1 = ParamUtil::Get($_REQUEST,'data',null,true);
     * $data2 = ParamUtil::Get($_REQUEST,'data2',null,true);
     * $data3 = ParamUtil::Get($_REQUEST,'data3',null,true);
     * $data4 = ParamUtil::Get($_REQUEST,'data',null,false);
     * echo $data1['b'];
     * Output:2
     * echo $data2['f'];
     * Output:6
     * var_dump($data3);
     * Output:NULL
     * echo $data4;
     * Output:{"json":{"a":1,"b":2,"c":3}}
     * ```
     * @param array $src Assoc array of parameters
     * @param string $param Name of key in assoc source
     * @param mixed $default (optional default: null) value to return if key is not found
     * @param bool $detectJSON if true will look for a {"json": or {'json': prefix and parse the value if prefix is found.
     * @return mixed value if found or default.
     * @throws \Exception "is not an array" thrown if src is not an array.
     * @see /utils/ParamUtil::SourceCheck
     */
    public static function Get($src, $param, $default = null, $detectJSON = false) {
        if (count($src) == 0)
            return $default;
        self::SourceCheck($src);

        $keys = implode(',', array_keys($src));
        $keys = strtolower($keys);
        $keys = explode(',', $keys);

        $index = array_search(strtolower($param), $keys);
        $keys = array_keys($src);
        if ($index === false)
            return $default;

        if (!isset($src[$keys[$index]]))
            return $default;

        $val = trim($src[$keys[$index]]);

        if ($detectJSON) {
            //correct single quoted json prefix
            if( stripos($val, "{'json':") === 0) {
                $val = '{"json":'. substr($val,8);                
                
            }
            if (stripos($val, '{"json":') === 0) {
                
                $val = self::GetJSON($src, $keys[$index], $default);
                if(!$val) return $val;
                return $val['json'];
            } 
        }
        return $val;
    }

    /**
     * Retrieve a parameter by passing possible paramter names. Allows for aliasing
     * a parameter name, or updating parameter names for backward compatability.
     * 
     * This is somewhat similar to unix -p "do somethign" vs --parameter="do something"
     * 
     * Usage:
     * ```php
     * ?p=do something
     * 
     * $param = ParamUtil::GetOne($_REQUEST,'p','parameter');
     * echo $param;
     * Output:do something
     * 
     * ?parameter=do something
     * $param = ParamUtil::GetOne($_REQUEST,'p','parameter');
     * echo $param;
     * Output:do something
     * 
     * $param = ParamUtil::GetOne($_REQUEST,'p','param');
     * var_dump($param);
     * Output: NULL
     * ```
     * @param array $src Assoc array of parameters
     * @param string param1...paramN one or more parameter names options to require from src
     * @return mixed value of target parameter or NULL
     * @throws \Exception "is not an array" thrown if src is not an array.
     */
    public static function GetOne($src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);
        $keys = array_keys($src);

        foreach ($args as $key) {
            if (in_array($key, $keys)) {
                return $src[$key];
            }
        }
        return null;
    }
    
     /**
     * Given a source array, get a new array with just the subset of parameters
     * specified. Ignore missing keys.
     * 
     * Usage:
      * ```php
     * ?param1=val1,param2=val2
     * $params = ParamUtil::GetAllValues($_REQUEST,'param1','param2','param3');
     * var_dump($params);
     * 
     * Output: array(2) { ["param1"]=> string(4) "val1" ["param2"]=> string(4) "val2" }
     * 
     * ```
     * @param array $src Assoc array of parameters
     * @param string $param1,...$paramN one or more names to get in src
     * @return array Assoc array with subset of data requested
     * 
     */
     public static function GetValues(array $src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);
        $values = array();

        foreach ($args as $arg) {
            if (isset($src[$arg])) {
                $values[$arg] = $src[$arg];
            } else {
                //$values[$arg] = null;
            }
        }

        return $values;
    }
    
    /**
     * Given a source array, get a new array with just the subset of parameters
     * specified.
     * 
     * Usage:
     * ```php
     * ?param1=val1,param2=val2
     * $params = ParamUtil::GetAllValues($_REQUEST,'param1','param2','param3');
     * var_dump($params);
     * 
     * Output: array(3) { ["param1"]=> string(4) "val1" ["param2"]=> string(4) "val2" ["param3"]=> NULL }
     * ```
     * 
     * @param array $src Assoc array of parameters
     * @param string $param1,...$paramN one or more names to get in src
     * @return array Assoc array with subset of data requested
     * 
     */
    public static function GetAllValues(array $src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);
        $values = array();

        foreach ($args as $arg) {
            if (isset($src[$arg])) {
                $values[$arg] = $src[$arg];
            } else {
                $values[$arg] = null;
            }
        }

        return $values;
    }
    
    /**
     * Sets the value of a parameter in the source parameter array to a provided
     * default and returns the value stored.
     * 
     * Warning: source array will be modified!
     *
     * Usage:
     * ```php
     * ParamUtil::ForceOne($_REQUEST,'authenticated',true);
     * 
     * or
     * 
     * $authenticate = ParamUtil::ForceOne($_REQUEST,'authenticated',true);
     * if($authenticate === ParamUtil::Get($_REQUEST,'authenticated) // force one worked.
     * ```
     * 
     * @param array $src Assoc array of parameters
     * @param string $param key to test for in assoc source
     * @param mixed $val value to set
     * @return mixed should be same value provided for default
     * @throws \Exception "is not an array" thrown if src is not an array.
     * @see /utils/ParamUtil::SourceCheck
     */
    public static function ForceOne(&$src, $param, $val) {
        self::SourceCheck($src);
        $src[$param] = $val;
        return $src[$param];
    }

    /**
     * Similar to Get, attempts to parse $param in $src as JSON into an assoc array
     * returns empty array on failure or if nothing provided.
     * 
     * 
     * Unlike the JSON detection in Get, GetJSON does not require a {"json": prefix.
     * 
     * Usage:
     * ```php
     * ?&action=list&data={"a":1,"b":2,"c":3}&bad={"a":"b":2,"c"}
     * 
     * $data = ParamUtil::GetJSON($_REQUEST,'data');
     * $data2 = ParamUtil::GetJSON($_REQUEST,'data2','doh!');
     * $data3 = ParamUtil::GetJSON($_REQUEST,'data3');
     * $bad = = ParamUtil::GetJSON($_REQUEST,'bad','that was bad');
     * 
     * echo $data['c'];
     * Output:3
     * 
     * echo $data2;
     * Output:doh!
     * 
     * var_dump($data3);
     * Output:array(0) { }
     * 
     * echo !is_null($bad) ? $bad : 'bad is null';
     * Output:false
     * 
     * Note that the bad parameter is malformed JSON so json_decode would return
     * null. Null is returned rather than thee default value since technically the
     * parameter did exist.
     * ```
     * 
     * @param array $src Assoc array of parameters
     * @param string $param key to test for in assoc source
     * @param mixed $default value to set
     * @return array|NULL assoc array, ether as empty array or other default, the result from json_decode (NULL or an array).
     * @throws \Exception "is not an array" thrown if src is not an array.
     * @see /utils/ParamUtil::SourceCheck
     * @see /utils/ParamUtil::Get
     */
    public static function GetJSON($src, $param, $default = array()) {
        self::SourceCheck($src);
        if (!isset($src[$param]))
            return $default;

        $srcContent = $src[$param];
        $content = json_decode($srcContent, true);
        
        if (!$content)
            return $content;
        return $content;
    }

    /**
     * Given a parameter source and a one or more targets to get, will return
     * a linear array with the values for each target found.
     * 
     * The values are interpreted as booleans whether or not the values are
     * actually boolean;
     * A false value is one of:
     * NULL , false, 'f', 'false', or 0
     * 
     * 
     * Usage:
     * ```php
     * ?is_filtering=true&is_listing=t&is_sorting=1
     * $request = $_REQUEST;
     * $request['yup'] = true;
     * $request['nope'] = false;
     * $request['is_a_thing'] = 'f';
     * $request['is_tracking'] = 'maybe';
     * 
     * ?is_listing=1&is_sorting=true list($isListing,$isFiltering,$isSorting,$yup,$nope,$isAThing,$isTracking,$numericFalse) = 
     *       ParamUtil::GetBoolean($request,'is_listing','is_filtering','is_sorting','yup','nope','is_a_thing','is_tracking','numeric_false');
     *
     * if($isFiltering && $isListing && $isSorting && $yup && $isTracking) echo 'all true';
     * // Output:all true
     
     * if(!$nope && !$isAThing  && !$numericFalse && !$notAThing) echo 'all false';
     * // Output:all false
     *
     * if(is_null($notAThing) echo 'null value'
     * Output:null value
     * ```
     * Note: all the true variations are redundant as any non false version would evaluate true
     * which includes the value 'maybe' given to is_tracking.
     *      
     * 
     * @param array $src Assoc array of parameters
     * @param $param1..n one or more additional targets to attempt to get from src
     * @return array an array of just the values so that list() may be used.
     * @throws \Exception "is not an array" thrown if src is not an array.
     * 
     */
    public static function GetBoolean($src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);
        $vals = array();
        foreach ($args as $arg) {
            if (isset($src[$arg])) {
                $val = $src[$arg];
                if(in_array($val,array(false,0,'0','f','false',null),true)) {
                    $vals[$arg] = false;
                } else {
                    $vals[$arg] = true;
                }
                
                
            } else {
                $vals[$arg] = false;
            }
        }
        return array_values($vals);
    }

    /**
     * Get a parameter and split it with a specified delimiter.
     * This is useful for splitting comma or other separated values provided
     * as arguments into a usable array.
     * 
     * Usage:
     * ```php
     * ?names=Larry,Moe,Curly
     * $names = ParamUtil::GetList($_REQUEST,',','names')
     * 
     * echo $names[2];
     * Output:Curly
     *```
     * @param array $src Assoc array of parameters
     * @param string $delim
     * @param string $param
     * @throws \Exception "is not an array" thrown if src is not an array.
     * @return array Linear array of separate values.
     */
    public static function GetList($src, $delim, $param) {
        self::SourceCheck($src);
        
        if (!isset($src[$param]))
            return array();
        return explode($delim, $src[$param]);
    }

    /**
     * Get one or more parameters and require that they have a value or
     * an exception is thrown.
     * 
     * The idea here is to provide functions witht he ability to identify required
     * parameters at the point where a parameter is used.
     * 
     * Usage:
     * ```php
     * ?param=someval
     * try {
     *   list($param,$param2) = ParamUtil::Requires($_REQUEST,'param','param2');
     * } catch( \Exception $e) {
     *   $message = $e->getMessage();
     *   echo($message);
     * } 
     * Output:Missing required parameters: param2
     * ```
     * @param array $src Assoc array of parameters
     * @return array list of values in the order of the provided params
     * @throws \Exception "is not an array" thrown if src is not an array.
     * @throws \Exception "Missing required parameters: ..." thrown if param names are not present, includes list of param names.
     * 
     */
    public static function Requires(array $src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);

        $keys = array_keys($src);
        $missing = array();
        $values = array();
        foreach ($args as $arg) {
            if (!in_array($arg, $keys)) {
                $missing[] = $arg;
                continue;
            }
            $val = trim($src[$arg]);
            if (stripos(substr($val, 0, 6), 'json')) {
                $val = json_decode($val, true);
                $val = $val['json'];
            }
            $values[] = $val;
        }
        if (count($missing))
            throw new \Exception('Missing required parameters: ' . implode(',', $missing));
        return $values;
    }

    /**
     * This variant of Requires allows specification of multiple parameter names
     * requiring that one of them is present.
     * 
     * This is for situations where you may want to alias required parameter names.
     * Or, perhaps you want to rename a parameter to be somethign more meaningful
     * but keep the old parameter name for backward-compatability.
     * 
     * Usage
     * 
     * ?n=10&q=1
     * ```php
     * $numLines = ParamUtil::RequiresOne($_REQUEST,'n','num_lines');
     * echo $numLines;
     * ```
     * Output:10
     * 
     * ?num_lines=10&q=1
     * ```php
     * $numLines = ParamUtil::RequiresOne($_REQUEST,'n','num_lines');
     * ```
     * Output:10
     * 
     * ?number_of_lines=10&q=1
     * ```php
     * try {
     *  $numLines = ParamUtil::RequiresOne($_REQUEST,'n','num_lines');
     * } catch (\Exception $e) {
     *   $message = $e->getMessage();
     *   echo($message);
     * } 
     * ```
     * Output:Missing required parameters: requires one of n,num_lines
     * 
     * @param array $src Assoc array of parameters
     * @param string param1...paramN one or more parameter names options to require from src
     * @return mixed value of required parameter
     * @throws \Exception "is not an array" thrown if src is not an array.
     * @throws \Exception "Missing required parameters: requires one of ..." thrown 
     * if at least one provided name is not present, includes comma-separated list of provided param names.
     */
    public static function RequiresOne(array $src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);
        
        $keys = array_keys($src);
        
        $matches = 0;
        foreach ($keys as $key) {

            if (in_array($key, $args)) {
                #var_dump($key,$args);
                $matches ++;
                break;
            }
        }
        
        if ($matches == 0)
            throw new \Exception('Missing required parameters: requires one of ' . implode(',', $args));
        return $src[$key];
    }
    /**
     * Require variant of GetJSON. Allows requiring a parameter and interpreting
     * the content as JSON in one operation.
     * 
     * Usage:
     * ?&action=list&data={"a":1,"b":2,"c":3}&bad={"a":"b":2,"c"}
     * ```php
     * $data = ParamUtil::RequireJSON($_REQUEST,'data');
     * $bad = = ParamUtil::RequireJSON($_REQUEST,'bad','that was bad');
     * 
     * echo $data['c'];
     * ```
     * Output:3
     * ```php
     * echo !is_null($bad) ? $bad : 'bad is null';
     * ```
     * Output:false
     * 
     * ```php
     * try {
            
            $data2 = ParamUtil::RequireJSON($request,'data2');
        } catch( \Exception $e) {
            echo $e->getMessage();            
        }
     * ```
     * Output:Missing required parameters: requires one of data2
     * 
     * @param array $src Assoc array of parameters
     * @param string $param key to test for in assoc source
     * @param mixed $default value to set
     * @return array|NULL assoc array, ether as empty array or other default, the result from json_decode.
     * @throws \Exception "is not an array" thrown if src is not an array.
     * @throws \Exception "Missing required parameters: requires one of ..." thrown 
     */
    public static function RequireJSON($src,$param) {
        //Note: SourceCheck is performed in RequiresOne
        $content = self::RequiresOne($src, $param);
        return json_decode($content, true);
    }
    
    /**
     * Array pruning helper to remove keys that are not needed
     * 
     * Warning: This will modify the source array.
     * 
     * For situations where an assoc array is obtained that has been decorated
     * by any number of function calls it was used with, it sometimes becomes
     * necessary to remove some un-needed keys rather than building a new array.
     * 
     * Usage
     * ?action=list&target=obj1&tracking=12345&cachebust=42
     * ```php
     * ParamUtil::Prune($_REQUEST,'tracking','cachebust');
     * var_dump($_REQUEST);
     * ```
     * Output: array(2) { ["action"]=> string(4) "list" ["target"]=> string(4) "obj1" }
     *
     * @param array $src Assoc array of parameters
     * @param key1...keyN  keys to remove from array
     * @throws \Exception "is not an array" thrown if src is not an array
     * @throws \Exception "Assoc array expected: consider using PruneIndexed instead" thrown if src is not an assoc array.
     */
    public static function Prune( &$src) {
        self::SourceCheck($src);
        if(!self::IsAssoc($src)) {
            throw new \Exception('Assoc array expected: consider using PruneIndexed instead');
        }
        $args = func_get_args();
        array_shift($args);

        foreach ($args as $key) {
            if (isset($src[$key]))
                unset($src[$key]);
        }
    }
    
   /**
    * Linear array variant of Prune
    * 
    * Warning: modifies source array
    * 
    * Remove a set of numeric index keys from a given linear array.
    * 
    * This function should safely remove entries from the provided array by
    * first sorting the provided indices and then removing them from highest
    * index to lowest index. 
    * 
    * It is also 'safe' in the sense it only unsets an index that actually exists
    * and will not complain 
    * 
    * Usage:
    * ```php
    * $fruit = array("apple","peaches","pumpkin","pie","cheese");
    * ParamUtil::PruneIndexed($fruit,[4,3]);
    * var_dump($fruit);
    * ```
    * Output: array(3) { [0]=> string(6) "apples" [1]=> string(7) "peaches" [2]=> string(7) "pumpkin" }
    * 
    * @param array $src linear array of parameters
    * @param array indices linear array of numeric index numbers to remove from the source array
    * @throws \Exception "is not an array" thrown if src is not an array
    * @throws \Exception "Linear array expected: Consider using Prune instead" thrown if src is an assoc array.
    * @see utils\ParamUtil::PruneIndexedValues
    * @see utils\ParamUtil::Prune
    */
    public static function PruneIndexed(&$src, $indices) {
        self::SourceCheck($src);
        
        if(self::IsAssoc($src)) {
            throw new \Exception('Linear array expected: Consider using Prune instead');
        }
        
        // Put the indices in order and loop through in reverse order
        // to safely remove things from the bottom up.
        sort($indices);
        
        $count = count($indices);
        while($count > 0) {
            $index = $indices[$count-1];
            
            if(isset($src[$index])) {
                unset($src[$index]);
            }
            $count-=1;
        }
    }

    /**
    * Linear array variant of Prune and PruneIndexed which allows you to prune by value
    * 
    * Warning: modifies source array
    * 
    * Remove a set of numeric index keys from a given linear array.
    * 
    * The indices for parameters provided are determined then passed to ParamUtil::PruneIndices
    * 
    * Usage:
     * ```php
    * $fruit = array("apple","peache","pumpkin","pie","cheese");
    * ParamUtil::PruneIndexeValues($fruit,"pie","cheese");
    * var_dump($fruit);
    * ```
    * Output: array(3) { [0]=> string(6) "apples" [1]=> string(7) "peaches" [2]=> string(7) "pumpkin" }
    * 
    * @param array $src linear array of parameters
    * @param mixed $val1,...$valN each values to remove from the linear array.
    * @throws \Exception "is not an array" thrown if src is not an array
    * @throws \Exception "Linear array expected: consider using Prune instead" thrown if src is an assoc array.
      @see utils\ParamUtil::PruneIndexedValues
    * @see utils\ParamUtil::Prune
    */
    public static function PruneIndexedValues(&$src) {
        self::SourceCheck($src);
        if(self::IsAssoc($src)) {
            throw new \Exception('Linear array expected: Consider using Prune instead');
        }
        
        $args = func_get_args();
        array_shift($args);
        $indices  = array();
        foreach ($args as $val) {
            $indices = array_merge($indices,array_keys($src,$val,true));            
        }
        self::PruneIndexed($src,$indices);
    }
    
    /**
     * This is a variant of GetAllValues that allows for aliased param names
     * using the convention "actual param name:aliased param name". As is true
     * with GetAllValues null values are included in the resulting assoc array.
     * 
     * Sometimes you get data from a source that has property names that are 
     * terse for computational purposes but are human-meaningless and vice versa.
     * 
     * Consider a table with fields f1 and q2 when we query the table we might
     * want to transform f1 as "First quarter earnings" and then q2 as "Second quarter earnings".
     * 
     * When we get data modified by the user back for a record retrieved with aliases
     * we would need to undo the aliases.
     * 
     * ```php
     * // simulate retrieved record from somewhere
     * $record = array('f1'=>1000.00,'q2'=>2000.0);
     * $outRecord = ParamUtil::GetAliasedValues($record,'f1:First quarter earnings','q2:Second quarter earnings','q3:Third quarter earnigns');
     * var_dump($outRecord);
     * ```
     * Output: array(3) { ["First quarter earnings"]=> float(1000) ["Second quarter earnings"]=> float(2000) ["Third quarter earnigns"]=> NULL }
     * 
     * ```php
     * // Simulate modification by some other functionality or as data submitted to server:
     * $inRecord = $outRecord;
     * $inRecord['Second quarter earnings'] = 1500.00;
     * 
     * $recordToStore = ParamUtil::GetAllAliasedValues($inRecord,'First quarter earnings:f1','Second quarter earnings:q2','Third quarter earnigns:q3');
     * var_dump($recordToStore);
     * ```
     * Output: array(3) { ["f1"]=> float(1000) ["q2"]=> float(1500) ["q3"]=> NULL }
     * 
     * Note: it is probably more efficient to get aliased values from an SQL database
     * by doing it at the SQL rather than per record write to the output stream.
     * The example above ws for illustrative purposes only.
     * 
     * @param array $src Assoc array of parameters
     * @param key1...keyN  keys to remove from array
     * @return array Assoc array for requested params including null parameters.
     * @throws \Exception "is not an array" thrown if src is not an array
     */
    public static function GetAllAliasedValues($src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);
        $values = array();
        $missing = array();
        foreach ($args as $arg) {

            $arg = explode(':', $arg);
            $as = '';
            if (count($arg) == 2) {
                list ($arg, $as) = $arg;
            } else {
                $arg = array_shift($arg);
                $as = $arg;
            }
            if (isset($src[$arg])) {

                $values[$as] = $src[$arg];
            } else {
                $values[$as] = null;
            }
        }

        return $values;
    }
    
    /**
     * This is a variant of GetValues that allows for aliased param names
     * using the convention "actual param name:aliased param name". As is true
     * with GetAllValues null values are included in the resulting assoc array.
     * 
     * Sometimes you get data from a source that has property names that are 
     * terse for computational purposes but are human-meaningless and vice versa.
     * 
     * Consider a table with fields f1 and q2 when we query the table we might
     * want to transform f1 as "First quarter earnings" and then q2 as "Second quarter earnings".
     * 
     * When we get data modified by the user back for a record retrieved with aliases
     * we would need to undo the aliases.
     * 
     * ```php
     * // simulate retrieved record from somewhere
     * $record = array('f1'=>1000.00,'q2'=>2000.0);
     * $outRecord = ParamUtil::GetAliasedValues($record,'f1:First quarter earnings','q2:Second quarter earnings','q3:Third quarter earnigns');
     * var_dump($outRecord);
     * ```
     * Output: array(2) { ["First quarter earnings"]=> float(1000) ["Second quarter earnings"]=> float(2000) }
     * 
     * ```php
     * // Simulate modification by some other functionality or as data submitted to server:
     * $inRecord = $outRecord;
     * $inRecord['Second quarter earnings'] = 1500.00;
     * 
     * $recordToStore = ParamUtil::GetAliasedValues($record,'First quarter earnings:f1','Second quarter earnings:q2','Third quarter earnigns:q3');
     * var_dump($recordToStore);
     * ```
     * Output: array(2) { ["f1"]=> float(1000) ["q2"]=> float(1500) }
     * 
     * Note: it is probably more efficient to get aliased values from an SQL database
     * by doing it at the SQL rather than per record write to the output stream.
     * The example above ws for illustrative purposes only.
     * 
     * @param array $src Assoc array of parameters
     * @param key1...keyN  parameters to get from $src
     * @return array assoc array with just the entries that were not null.
     * @throws \Exception "is not an array" thrown if src is not an array
     */
    public static function GetAliasedValues($src) {
        self::SourceCheck($src);
        $args = func_get_args();
        array_shift($args);
        $values = array();
        $missing = array();
        foreach ($args as $arg) {

            $arg = explode(':', $arg);
            $as = '';
            if (count($arg) == 2) {
                list ($arg, $as) = $arg;
            } else {
                $arg = array_shift($arg);
                $as = $arg;
            }
            if (isset($src[$arg])) {

                $values[$as] = $src[$arg];
            } else {
                //$values[$as] = null;
            }
        }

        return $values;
    }

    /**
     * For an explanation of aliased values see the documentation for 
     * ParamUtil::GetAliasedValues
     * 
     * This is the Require variant meaning if any parameters are not found, an
     * exception will be thrown.
     * 
     * Usage:
     * ```php
     * $record = array('f1'=>1000.00,'q2'=>2000.0);
     * $outRecord = ParamUtil::RequireAliasedValues($record,'f1:First quarter earnings','q2:Second quarter earnings');
     * var_dump($outRecord);
     * ```php
     * Output: array(2) { ["First quarter earnings"]=> float(1000) ["Second quarter earnings"]=> float(2000) }
     * 
     * ```php
     * try {
     *  $outRecord = ParamUtil::RequireAliasedValues($record,'f1:First quarter earnings','q2:Second quarter earnings','q3:Third quarter earnings');
     * } catch( \Exception $e ) {
     *   echo $e->getMessage();
     * }
     * ```
     * Output:Missing required parameters: q3
     * 
     * @param array $src Assoc array of parameters
     * @param key1...keyN  keys to remove from array
     * @return array Assoc array of required parameters
     * @throws \Exception 'Missing required parameters: ' includes comma separated list of missing parameters
     * @see \utils\ParamUtil::GetAliasedValues
     * 
     */
    public static function RequireAliasedValues($src) {
        $args = func_get_args();
        array_shift($args);
        $values = array();
        $missing = array();
        foreach ($args as $arg) {
            $arg = explode(':', $arg);
            $as = '';
            if (count($arg) == 2) {
                list ($arg, $as) = $arg;
            } else {
                $arg = array_shift($arg);
                $as = $arg;
            }
            if (isset($src[$arg])) {

                $values[$as] = $src[$arg];
            } else {
                $missing[] = $arg;
            }
        }
        if (count($missing))
            throw new \Exception('Missing required parameters: ' . implode(',', $missing));
        return $values;
    }

    /**
     * ParseParams is meant to aid in parsing vanity URL paths that have
     * structure like:
     * /param1:val1/param2:val2/
     * 
     * It can either take the url string or an array of "key:val" pair strings.
     * 
     * The function builds a json object string based on  he provided src path,
     * url decodes it, then json decodes it into an actual assoc array.
     * 
     * While it may be used directly, it is more intended for use by a central
     * index.php or global include file which recieves the vanity path and then
     * uses ParamUtil to parse the params for inclusion in $_REQUEST.
     * 
     * Usage: 
     * ```php
     * $url = "/param1:val1/param2:val%202/";
     * $params = ParamUtil::ParseParams($url);
     * var_dump($params);
     * ```
     * Output:array(2) { ["param1"]=> string(4) "val1" ["param2"]=> string(5) "val 2" }
     * 
     * @param array|string $src parameterized part of path string or array of "key:val" pair strings.
     * @return array assoc array
     */
    public static function ParseParams($src) {
        $params = "{";
        $firstVal = true;
        if (!is_array($src))
            $src = explode('/', $src);
        foreach ($src as $data) {
            if ($data == "")
                continue;
            $params .= $firstVal ? '' : ',';
            if (strpos($data, ':')) {
                list ($key, $val) = explode(':', $data);
                $key = "\"$key\"";
                if (preg_match('/\D/', $val)) {
                    $val = "\"$val\"";
                }
                $data = "$key:$val";
                $params .= $data;
            } else {
                if (preg_match('/\D/', $data)) {

                    $params .= "\"$data\"";
                } else {
                    $params .= $data;
                }
            }

            $firstVal = false;
        }
        $params .= '}';
        
        $params = urldecode($params);

        $params = json_decode($params, true);
        return $params;
    }

    /**
     * Determine if a source array is an assoc array by looking at its keys
     * and detecting non numeric keys.
     * 
     * Usage:
     * ```php
     * $test = array('a'=>1,0=>'stuff');
     * if(ParamUtil::IsAssoc($test)) {
     *  echo "is assoc";
     * }
     * ```
     * Output:is assoc
     *
     * ```php
     * $test = array('stuff','morestuff');
     * if( ParamUtil::IsAssoc($test) === false) {
     *  echo "is not assoc";
     * }
     * ```
     * Output:is assoc
     * @param array $src
     * @return boolean true if is assoc false if not
     */
    public static function IsAssoc($src) {
        if (!is_array($src))
            return false;
        $keys = preg_grep('/[^\d]/', array_keys($src));
        return count($keys) > 0;
    }

   /**
    * Given a linear list of assoc arrays, get a list based on single property
    * common in all assoc arrays.
    * 
    * Useful when you have something like an array of records from the same 
    * table and you want the values for a column
    * 
    * Also allows for obtaining nested sub values using dot-syntax see second 
    * usage example
    * 
    * Usage:
    * ```php
    * $data=array();
    * $data[] = array('id'=>'12340','field1'=>'val1');
    * $data[] = array('id'=>'12341','field1'=>'val2');
    * $data[] = array('id'=>'12342','field1'=>'val3');
    * 
    * $ids = ParamUtil::GetSubValues($data,'id');
    * var_dump($ids);
    * ```
    * Output: array(3) { [0]=> string(5) "12340" [1]=> string(5) "12341" [2]=> string(5) "12342" }
    * 
    * ```php
    * $data=array();
    * $data[] = array('id'=>'12340','field1'=>array('key'=>'val'));
    * $data[] = array('id'=>'12341','field1'=>array('key'=>'val2'));
    * $data[] = array('id'=>'12342','field1'=>array('key'=>'val3'));
    * 
    * $ids = PramUtil::GetSubValues($data,'field1.key');
    * var_dump($ids);
    * ```
    * Output: array(3) { [0]=> string(3) "val" [1]=> string(4) "val2" [2]=> string(4) "val3" }
    * 
    * 
    * @param array $src linear array of assoc arrays
    * @param string $subId id to check for in each item in $src
    * @return array linear array representing a "column" of data.
    * @throws \Exception "is not an array" thrown if src is not an array
    */
    public static function GetSubValues($src, $subId, $excludeNull=false) {
        self::SourceCheck($src);
        $subs = array();
        $subId = explode('.', $subId);
        foreach ($src as $item) {
            $subItem = $item;
            foreach ($subId as $id) {
                $subItem = $subItem[$id];
            }

            if (isset($subItem)) {
                $subs[] = $subItem;
            } else {
                if(!$excludeNull) {
                    $subs[] = null;
                }
            }
        }
        return $subs;
    }
     
    /**
     * Get a new array (without modifying the source array) with just the 
     * keys you want.
     * 
     * Usage:
     * ?action=list&target=obj1&tracking=12345&cachebust=42
     * ```php
     * $params = ParamUtil::SubsetAssocArray($_REQUEST, array('action','target'));
     * var_dump($_REQUEST);
     * ```
     * Output: array(4) { ["action"]=> string(4) "list" ["target"]=> string(4) "obj1" ["tracking"]=> string(5) "12345" ["cachebust"]=> string(2) "42" }
     * 
     * @param array $src
     * @param array $keys
     * @return array
     */
    public static function SubsetAssocArray($src, $keys) {
        $subset = array();
        foreach ($keys as $key) {
            // if(!in_array($key,$keys)) continue;
            $val = $src[$key];
            $subset[$key] = $val;
        }
        return $subset;
    }

    /**
     * Get t or f for true or false
     * Useful for setting boolean values in PostGRES
     * 
     * Usage:
     * ```php
     * $isValid = true;
     * 
     * $t_or_f = ParamUtil::BoolToTF($isValid);
     * 
     * echo $t_or_f;
     * ```
     * Output:t
     * 
     * @param type $boolVar
     * @return boolean
     */
    public static function BoolToTF($boolVar = null) {
        return ($boolVar) ? 't' : 'f';
    }

    /**
     * One of the more common tasks for REST service requests is checking
     * for a format, generally xml or json, GetFormat defaults to json unless
     * otherwise specified.
     * 
     * Usage:
     * ?format=html
     * ```php
     * $format = ParamUtil::GetFormat($_REQUEST);
     * echo $format;
     * ```
     * Output:html
     * 
     * ```php
     * ?action=list
     * $format = ParamUtil::GetFormat($_REQUEST);
     * echo $format 
     * ```
     * Output:json
     * 
     * 
     * @param type $params
     * @param type $format
     * @return type
     */
    public static function GetFormat($params, $format = 'json') {
        return self::Get($params, 'format', $format);
    }

 
}

?>
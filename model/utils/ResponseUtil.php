<?php

namespace utils;

/**
 * 
 * ResponseUtil
 * 
 * ResponseUtil is meant to help create resposnses in REST endpoints.
 * Given a format and the current action it is possible to 
 * send back simple responses, or results either from an array/object or 
 * from an ADORecordSet (results from querying the database).
 * 
 * ResponseUtil will support JSON or XML output. Using this class it is
 * possible to send data back in common formats without having to know much
 * about them.
 * 
 * As far as writing data out is concerned the ResponseUtil functionality
 * has an optimization in that it doesn't build the response in memory and then
 * send it, rather it writes out stuff to the client as it goes. 
 * 
 */
class ResponseUtil {

    const FORMAT_JSON = "json";
    const FORMAT_XML = "xml";
    const IS_FIRST = true;
    const NOT_FIRST = false;
    const FULL_RESPONSE = true;
    const ACTION_ONLY = false;
    const AS_ARRAY = false;
    const AS_AUTO = 'auto';
    const AS_OBJ = true;
    const STATUS_OK = true;
    const NO_VALUE = null;

    private $format = self::FORMAT_JSON;
    private $action;
    private $includeHeaders = true;
    private $started = false;
    private $resultsStarted = false;
    private $resultsAsObj = false;
    private $resultCount = 0;
    private $properties = null;
    private $responseEnded = false;
    private $resultsEnded = false;

    /**
     * Constructor function
     * 
     * When the object is initially constructed it does not actually begin
     * sending anything until it is explicitly told to do so.
     * 
     * Example
     * 
     * $response = new ResponseUtil('json','myendpoint/action');
     * 
     * @param string $format 'json' or 'xml'
     * @param string $action the endpoint and action that was called
     * @param boolean $includeHeaders generate html headers for the response, this should only be done if you have not already sent headers or written back text.

     */
    public function __construct($format, $action, $includeHeaders = true) {
        $this->format = $format;
        $this->action = $action;
        $this->includeHeaders = $includeHeaders;
    }

    /**
     * Properties are a set of key/vals that will be added automatically
     * when StartResponse is called.
     * 
     * @param mixed $props arry or object with key val data.
     */
    public function SetProps($props) {
        $this->properties = $props;
    }

    public static function RequireSessionUser() {

        $sessionUser = model\Users::GetSessionUser();
        if (!$sessionUser) {
            throw new \Exception('Session Error: No session or session user not known');
        }
        return $sessionUser;
    }

    /**
     * Begins the response by writing out the beginning content including the action.
     * 
     * If the object was constructed with includeHeaders set true this will also
     * take care of sending the Content-type header based on the format specified
     * at construction.
     * 
     * Output is sent directly to the output stream via echo.
     * 
     * 
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function StartResponse() {
        if ($this->started) {
            return $this;
        }
        switch ($this->format) {
            case 'xml':
                $action = $this->action;
                if ($this->includeHeaders) {
                    header("Content-type: text/xml");
                }
                echo '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
                $action = htmlentities($action);

                echo <<<XML
<response><action>$action</action>
XML;
                break;
            case 'json':
            default:
                $action = $this->action;
                if ($this->includeHeaders)
                    header("Content-type: application/json");

                $start = <<<JSON
{"response":{"action":"$action"
JSON;
                echo(trim($start));
                break;
        }
        $this->started = true;
        if ($this->properties !== null) {
            $val = "";
            foreach ($this->properties as $key => $val) {
                if($key == 'results') {
                    $this->StartResults();
                    $this->WriteResults($val);
                    $this->EndResults();
                    continue;
                }
                $this->WriteKeyVal($key, $val);
            }
        }
        return $this;
    }

    /**
     * Send an error message back to the client and sets the status for the
     * response to "error"
     * 
     * You can also opt to end the response by passing true for the fullResponse
     * parameter.
     * 
     * Example 1:
     * 
     * $response = new ResponseUtil('json','myendpoint/action');
     * $response->StartResponse();
     * 
     * try {
     *  $result = 1/0;
     * } catch (Exception $e) {
     *  $response->SendError($e->getMessage(),self::ResponseUtil::FULL_RESPONSE);
     * }
     * 
     * 
     * Example 2
     * 
     * $response = new ResponseUtil('json','myendpoint/action');
     * $response->SendError('Missing parameter; why did you do that?',ResponseUtil::FULL_RESPOSE);
     * 
     * @param string $message Error message to send back to the client
     * @param boolean $fullResponse (default: ResponseUtil::ACTION_ONLY)
     * if ResponseUtil::FULL_RESPONSE a full response will be written. 
     * Options: ResponseUtil::FULL_RESPONSE or ResponseUtil::ACTION_ONLY
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function SendError($message, $fullResponse = self::ACTION_ONLY) {
        if ($fullResponse) {
            $this->StartResponse();
        }
        switch ($this->format) {
            case 'xml':
                $message = htmlentities($message);
                echo <<<XML
<status>error</status>
<error><![CDATA[$message]]></error>    
XML;
                break;
            case 'json':
            default:
                echo ",\"status\":\"error\"";
                echo ",\"message\":\"$message\"";
        }

        if ($fullResponse) {
            $this->EndResponse();
            return $this;
        }
        return $this;
    }

    /**
     * If sending back results, begin the results section of the response.
     * 
     * 
     * @param boolean $as (default ResponseUtil::AS_AUTO) only relevant when format
     * is json indicates whether the results parameter in the output should have
     * a value defined as an object {} or as an array [], auto will auto-detect based on the results. Options are 
     * ResponseUTIL::AS_AUTO, ResponseUtil::AS_ARRAY or ResponseUtil::AS_OBJ;
     * 
     * @see \model\ResponseUtil::WriteResults()
     * @see \model\ResponseUtil::EndResults();
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function StartResults($as = self::AS_AUTO) {
        if ($this->resultsStarted) {
            return $this;
        }
        $this->resultsAsObj = $as;


        switch ($this->format) {
            case 'xml':
                echo "<results>";
                break;
            default:
            case 'json':
                $squareOrCurly = '[';
                switch ($as) {
                    case self::AS_AUTO:
                        $squareOrCurly = "";
                        break;
                    case self::AS_ARRAY:
                        $squareOrCurly = "[";
                        break;
                    case self::AS_OBJ:
                        $squareOrCurly = "{";
                        break;
                }
                echo ",\"results\":" . $squareOrCurly;
                break;
        }

        return $this;
    }

    /**
     * Adds a key (property) to the result object (if JSON). If format is xml the key will
     * be an attribute on the <result> element.
     * 
     * @param string $key The property/attribute name to use.
     * @param mixed $value value to set for key. If an array or object
     * WriteKeyVal will be called recursively using the properties of the object
     * or keys of the array.
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function WriteKeyVal($key, $value) {
        if ($this->responseEnded)
            return $this;
        switch ($this->format) {
            case 'xml':
                if (is_array($value)) {
                    $keys = array_keys($value);
                    echo "<$key>";
                    if (is_string($keys[0])) {
                        foreach ($value as $k => $v) {
                            self::WriteKeyVal("item index=\"$k\"", $v);
                        }
                    } else {
                        $i = 0;
                        foreach ($value as $k => $v) {
                            self::WriteKeyVal("item index=\"$k\"", $v);
                        }
                    }
                    $key = explode(" ", $key);
                    $key = array_shift($key);
                    echo "</$key>";
                } else {

                    echo ("<$key>$value");
                    $key = explode(" ", $key);
                    $key = array_shift($key);
                    echo ("</$key>");
                }
                // $value = is_array($value) ? json_encode($value) : htmlentities($value);

                break;
            case 'json':
            default:
                echo ",\"$key\":" . json_encode($value);
                break;
        }
        return $this;
    }

    /**
     * 
     * Write a single result entry.
     * 
     * If the intent is to write multiple results, then it is important that for
     * the first item to indicate that it is first by passing true as the second
     * parameter
     * 
     * Note: isFirst is now optional, and will be autodetected if isFirst is not
     * set based on the $this->resultCount counter which will increment with
     * each write result.
     * 
     * @param mixed $result result record or object to output as a result item. 
     * @param boolean $isFirst (optional) is this result the first result? If format is json and true prevents extra syntax in output.
     * @param mixed $resultId (default null) more for xml will set a resultId on result element. If json will create a propry named based on id and set the result as the value for that propery.
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function WriteResult($result, $isFirst = null, $resultId = null) {
        if (is_a($result, '\model\records\CachedRecord')) {
            $result = $result->GetRecord();
        }
        if ($this->resultsEnded)
            return $this;
        if ($this->resultCount === 0) {
            $isFirst = true;
        }

        switch ($this->format) {
            case 'xml':

                $id = is_null($resultId) ? "" : $resultId;

                //var_dump((preg_match('/[^0-9]/', $id) == 1) ? 'true' : 'false');
                $id = (preg_match('/[^0-9]/', $id) == 1) ? "name=\"$resultId\"" : "index=\"$resultId\"";
                if ($id === "index=\"\"" || $id === "name=\"\"") {
                    $id = "";
                }

                if ($id != '') {
                    if($this->IsElementable($id)) {
                        echo "<$id ";
                    } else {
                        echo "<result $id ";
                    }
                    if (ParamUtil::IsAssoc($result) || is_object($result)) {
                        echo "type=\"dict\"";
                    } elseif (is_array($result)) {
                        echo "type=\"list\"";
                    }
                    echo ">";
                }
                if (!is_array($result) && !is_object($result)) {
                    $result = array(
                        $result
                    );
                }
                foreach ($result as $key => $val) {
                    $indexedItem = (preg_match('/[^0-9]/', $key) == 1);
                    $name = $indexedItem ? "name=\"$key\"" : "index=\"$key\"";

                    if (ParamUtil::IsAssoc($val) || is_object($val)) {

                        echo "<item $name type=\"dict\">";
                        $this->WriteResult($val, self::NOT_FIRST);
                        echo "</item>";
                    } else {
                        $atts = "";
                        $isInt = is_int($val);
                        $isFloat = is_float($val);
                        $isDouble = is_double($val);
                        $isBool = is_bool($val);
                        if ($isInt) {
                            $atts .= " type=\"int\" ";
                        } elseif ($isFloat) {
                            $atts .= " type=\"float\" ";
                        } elseif ($isDouble) {
                            $atts .= " type=\"double\" ";
                        } elseif ($isBool) {
                            $atts .= " type=\"bool\" ";
                        } else {
                            $atts .= "type=\"string\" ";
                            $val = htmlentities($val);
                            $val = "<![CDATA[" . $val . "]]>";
                        }
                        echo "<item $name $atts>$val</item>";                        
                    }
                }
                if ($id != '') {
                   if($this->IsElementable($id)) {
                        echo "</$id>";
                    } else {
                        echo "</result>";
                    }
                }
                break;
            case 'json':
            default:
                if (!$isFirst)
                    echo ",";
                if ($resultId) {
                    if (!is_numeric($resultId)) {
                        echo "\"$resultId\"" . ':';
                    }
                }
                $item = $result;
                if (is_array($result) || is_object($result)) {
                    echo json_encode($item);
                } else {
                    if (is_string($result))
                        $result = json_encode($result);
                    echo $result;
                    ;
                }

                break;
        }
        $this->resultCount += 1;
        return $this;
    }

    /**
     * Given the results from a db query, loop through the and write the 
     * results from the database.
     * 
     * @param ADOResultSet Results retrieved from an ADODB Execute (i.e. $db->Execute(...) or simlar call to the database.
     * @param boolean  $fullResponse (default ResponseUtil::ACTION_ONLY)
     * If set to ResponseUtil::FULL_RESPONSE a full response with resultset 
     * will be written.
     * Options: ResponseUtil::ACTION_ONLY or ResponseUtil::FULL_RESPONSE
     * 
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function WriteADOResults($resultCursor, $fullResponse = self::ACTION_ONLY) {
        if ($this->resultsEnded)
            ;
        if ($fullResponse) {
            $this->StartResponse();
        }
        $hasResults = !in_array($resultCursor, array(
                    false,
                    null
        ));

        if (!$hasResults) {
            $this->EndResults();
            if ($fullResponse) {
                $this->EndResponse();
            }
            return $this;
        }
        $isFirst = true;

        $this->StartResults(self::AS_ARRAY);
        foreach ($resultCursor as $result) {
            $this->WriteResult($result, $isFirst);
            $isFirst = false;
        }
        $this->EndResults();
        if ($fullResponse) {
            $this->WriteOkStatus();
            $this->EndResponse();
        }
        return $this;
    }

    /**
     * Given an iterator (array or object or anything that works with foreach
     * like and ADORecordSet) loop through results and write them out as result
     * entries.
     * 
     * @param \Iterator $results an array, object, or ADORecordset holding a set of records.
     * @param boolean $fullResponse (default ResponseUtil::ACTION_ONLY) If set to ResponseUtil::FULL_RESPONSE a full response with resultset will be written.
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function WriteResults($results, $fullResponse = false) {
        $isAssoc = ParamUtil::IsAssoc($results);
        $isArray = is_array($results);
        $isObj = is_object($results);

        if (!is_a($results, 'Iterator')) {

            if (is_a($results, '\model\records\CachedRecord')) {
                $results = array($results);
            }
        }

        if ($this->resultsAsObj === self::AS_AUTO) {
            $this->resultsAsObj = ($isAssoc || $isObj) ? self::AS_OBJ : self::AS_ARRAY;
            if ($this->format === self::FORMAT_JSON)
                echo $this->resultsAsObj == self::AS_OBJ ? '{' : '[';
        }

        if ($this->responseEnded)
            return $this;
        if ($fullResponse) {
            $this->StartResponse();
        }
        if ($fullResponse) {
            $this->StartResults();
        }
        if ($results === false)
            $results = [];
        $isFirst = true;

        foreach ($results as $key => $result) {
            $this->WriteResult($result, $isFirst, $key);
            $isFirst = false;
        }

        if ($fullResponse) {
            $this->EndResults();
            $this->WriteOkStatus();
            $this->EndResponse();
        }
        return $this;
    }

    /**
     * Adds a status element in xml or a property to the response json
     * named "status" with a value of "ok".
     * 
     * An optional message may be provided which will provide a status_message
     * property to go with the message.
     * 
     * @param string $message (optional) if provided a 
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function WriteOkStatus($message = null, $fullResponse = self::ACTION_ONLY) {

        if ($fullResponse) {
            $this->StartResponse();
        }

        $this->WriteKeyVal('status', 'ok');
        if (!is_null($message)) {
            $this->WriteKeyVal('status_message', $message);
        }

        $this->EndResponse();

        return $this;
    }

    /**
     * Send a full response, optionally with parameters and a status message to 
     * include with status=ok.
     * 
     * @param string $statusMessage (optional default ResponseUtil::NO_VALUE, value to use as status message in addition to status=ok)
     * @param type $optParams (optional default ResponseUtil::NO_VALUE), non-results info as assoc array or object to include with the response.
     * @return \utils\ResponseUtil
     */
    public function WriteSimpleResponse($optParams = self::NO_VALUE, $statusMessage = self::NO_VALUE) {
        if (!is_null($optParams)) {
            $this->SetProps($optParams);
        }
        $this->StartResponse()->WriteOkStatus($statusMessage);
        return $this;
    }

    /**
     * Write out the end to the results element/array.
     * 
     * @param boolean $asObj (optional default ResponseUtil::AS_ARRAY) 
     * relevant only for json, indicates how to end the results.
     * Options: ResponseUtil::AS_ARRAY or ResponseUtil::AS_OBJ
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function EndResults($asObj = null) {

        if ($this->resultsEnded)
            return $this;
        if (is_null($asObj)) {
            $asObj = $this->resultsAsObj;
        }

        switch ($this->format) {
            case 'xml':
                echo "</results>";
                break;

            case 'json':
            default:
                $squareOrCurly = '[';
                switch ($asObj) {
                    case self::AS_ARRAY:
                        $squareOrCurly = "]";
                        break;
                    case self::AS_OBJ:
                        $squareOrCurly = "}";
                }
                echo $squareOrCurly;
        }
        $this->resultsEnded = true;
        return $this;
    }

    /**
     * Writes the end of the response element/property.
     *  
     * @param boolean $statusOk (optional) if set to ResponseUtil::STATUS_OK
     * will include an ok status element/property before closing the response.
     * @return \utils\ResponseUtil current instance of object; allows chaining
     */
    public function EndResponse($statusOk = false) {
        if ($this->responseEnded)
            return $this;
        if ($statusOk) {
            $this->WriteOkStatus();
        }
        switch ($this->format) {
            case 'xml':
                echo '</response>';
                break;
            case 'json':
            default:
                echo "}}";
                break;
        }
        $this->responseEnded = true;
        return $this;
    }

    private function IsElementable($name) {
        //RegEx acquired from example from hakre's answer here:
        ////  https://stackoverflow.com/questions/2519845/
        $domElementRegex = '~^[:A-Z_a-z\\xC0-\\xD6\\xD8-\\xF6\\xF8-\\x{2FF}\\x{370}-\\x{37D}\\x{37F}-\\x{1FFF}\\x{200C}-\\x{200D}\\x{2070}-\\x{218F}\\x{2C00}-\\x{2FEF}\\x{3001}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFFD}\\x{10000}-\\x{EFFFF}][:A-Z_a-z\\xC0-\\xD6\\xD8-\\xF6\\xF8-\\x{2FF}\\x{370}-\\x{37D}\\x{37F}-\\x{1FFF}\\x{200C}-\\x{200D}\\x{2070}-\\x{218F}\\x{2C00}-\\x{2FEF}\\x{3001}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFFD}\\x{10000}-\\x{EFFFF}.\\-0-9\\xB7\\x{0300}-\\x{036F}\\x{203F}-\\x{2040}]*$~u';
        return \preg_match($domElementRegex, $name) > 0;
    }

    /**
     * Calls the die function and will prevent any additional php execution.
     */
    public function End() {
        die();
    }

}

?>